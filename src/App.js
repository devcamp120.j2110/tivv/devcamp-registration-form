import 'bootstrap/dist/css/bootstrap.min.css'
import Registration from './components/registration-form';

function App() {
  return (
    <div>
      <Registration/>
    </div>
  );
}

export default App;
