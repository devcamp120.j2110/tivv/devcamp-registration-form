import { Button, Col, Container, Input, Label, Row } from "reactstrap";
import '../App.css'

function Registration() {
    return (
        <Container>
            <Row className="mt-4">
                <h3 className="text-center">Registration Form</h3>
            </Row>
            <Row className="my-3">
                <Col sm='6'>
                    <Row>
                        <Col sm='2'>
                            <Label>First name <span>(*)</span></Label>
                        </Col>
                        <Col sm='10'>
                            <Input></Input>
                        </Col>
                    </Row>
                </Col>
                <Col sm='6'>
                    <Row>
                        <Col sm='2'>
                            <Label>Passport <span>(*)</span></Label>
                        </Col>
                        <Col sm='10'>
                            <Input></Input>
                        </Col>
                    </Row>
                </Col>
            </Row>
            <Row className="my-3">
                <Col sm='6'>
                    <Row>
                        <Col sm='2'>
                            <Label>Last name <span>(*)</span></Label>
                        </Col>
                        <Col sm='10'>
                            <Input></Input>
                        </Col>
                    </Row>
                </Col>
                <Col sm='6'>
                    <Row>
                        <Col sm='2'>
                            <Label>Email </Label>
                        </Col>
                        <Col sm='10'>
                            <Input></Input>
                        </Col>
                    </Row>
                </Col>
            </Row>
            <Row className="my-3">
                <Col sm='6'>
                    <Row>
                        <Col sm='2'>
                            <Label>Birth day <span>(*)</span></Label>
                        </Col>
                        <Col sm='10'>
                            <Input></Input>
                        </Col>
                    </Row>
                </Col>
                <Col sm='6'>
                    <Row>
                        <Col sm='2'>
                            <Label>Country <span>(*)</span> </Label>
                        </Col>
                        <Col sm='10'>
                            <Input></Input>
                        </Col>
                    </Row>
                </Col>
            </Row>
            <Row className="my-3">
                <Col sm='6'>
                    <Row>
                        <Col sm='2'>
                            <Label>Gender <span>(*)</span></Label>
                        </Col>
                        <Col sm='10' >
                            <Row>
                                <Col sm='6' className="text-center">
                                    <Input type='radio' name='gender' id="Male" />
                                    <Label htmlFor="Male" className="mr-4">Male</Label>

                                </Col>
                                <Col sm='6' className="text-center">
                                    <Input type='radio' name='gender' id="Female" />
                                    <Label htmlFor="Female" className="mr-4">Female</Label>

                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Col>
                <Col sm='6'>
                    <Row>
                        <Col sm='2'>
                            <Label>Region </Label>
                        </Col>
                        <Col sm='10'>
                            <Input></Input>
                        </Col>
                    </Row>
                </Col>
            </Row>
            <Row className="my-3">
                <Col sm='1'>
                    <Label>Subject</Label>
                </Col>
                <Col sm='11'>
                    <textarea className="form-control"></textarea>
                </Col>
            </Row>
            <Row>
                <Col sm='12' className="text-center">
                    <Button className="btn btn-success mx-2">Check Data</Button>
                    <Button className="btn btn-info mx-2">Send</Button>

                </Col>
            </Row>

        </Container>

    )
}
export default Registration;